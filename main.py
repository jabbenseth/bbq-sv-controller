from tornado.ioloop import IOLoop
from tornado.web import RequestHandler, Application, url
#from HeaterMeter import HeaterMeterWebinterface
from frontends.sousvide import Configuration, Dashboard, Historic

import tornado
import os


database = {}
db_sousvide = {'Thermo': None,
               'sensors': [],
               'debug': True,
               'config_dict': None}
static_path = os.path.join(os.path.dirname(__file__), "static")
config_path = os.path.join(os.path.dirname(__file__), "config")

settings = {"static_path": static_path,
            "config_path": config_path,
            "debug": True}


def make_app():
    return Application([
        #url(r"/BBQ/", HeaterMeterWebinterface, dict(database=database)),
        url(r"/", Dashboard, dict(database=db_sousvide)),
        url(r"/configuration/", Configuration,
            dict(database=db_sousvide)),
        url(r"/historic/", Historic),
        url(r"/static/(.*)", tornado.web.StaticFileHandler,
            {"path": static_path}),
        url(r"/config/(.*)", tornado.web.StaticFileHandler,
            {"path": config_path}),
    ],
        **settings)


def main():
    port = 8888
    app = make_app()
    app.listen(port)
    print('Start server at port %d' % port)
    try:
        IOLoop.instance().start()
    except KeyboardInterrupt:
        if db_sousvide['Thermo'] is not None:
            db_sousvide['Thermo'].shutdown()
    return 1

if __name__ == '__main__':
    main()
