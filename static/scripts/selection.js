sensorTemplate =""+
"<fieldset id='sensorFieldsetPort#ID' class='mdl-cell mdl-cell--1-col'>"+
" <legend id='legendTemplate'>Port #ID</legend>"+
"<label for='type#ID'>type</label>"+
" <select id='sensorTypeSelection#ID' name='type#ID'> </select>"+
"<div class='mdl-textfield mdl-js-textfield'>"+
" <label for='name#ID'>Name</label>"+
" <input type='text' id='name#ID' class='mdl-textfield__input' value='Sensor #ID'/>"+
"</div>"+
"role: <input type='radio' id='controlRole#ID' name='role#ID' value='Control'>"+
" <label for='controlRole#ID'> Control</label>"+
" <input type='radio' id='observeRole#ID' name='role#ID' value='Observe' checked='True'>"+
" <label for='observeRole#ID'>  Observe</label><br>"+
"<div class='mdl-textfield mdl-js-textfield'>"+
"<label for='tSet#ID' >TSet</label>"+
"<input type='number' class='mdl-textfield__input' step='0.5' id='tSet#ID' name='tSet#ID' />"+
"</fieldset>";
additionalTempsTemplate = ""+
"<label for='tHigh#ID' id='labelTHigh#ID'>THigh</label>"+
" <input type='number' class='mdl-textfield__input' step='0.5' id='tHigh#ID' name='tHigh#ID'/>"+
"<label for='tLow#ID' id='labelTLow#ID'>TLow</label>"+
"<input type='number' class='mdl-textfield__input' step='0.5' id='tLow#ID' name='tLow#ID'/>";
analog_sensors = [];
digital_sensors = [];
config_dict = {};

do_thing = true

$(document).on('change','input[type=checkbox][id^=mono_stereo]', function () {
    if (!do_thing){return;}
    var n =this.id.slice(-1);
    var port = (n*2)+4;
    var id = ('#port-'+port);

    if (!this.checked) {
        $(id).removeAttr("disabled");
    }
    else{
        $(id).attr('disabled', true);
        if ($('#port-'+(port-1)).prop('checked') && $('#sensorFieldsetPort'+port).length==0)
        {
            $(id).prop('checked',true);
            createSensorFieldset($(id)[0]);
        }
        else if (!$('#port-'+(port-1)).prop('checked'))
        {
            $(id).prop('checked',false);
            createSensorFieldset($(id)[0]);

        }
    }
});


$(document).on('change','input[type=radio][name^=role]', function () {
    if (this.value == 'Observe') {
        removeAdditionalTemperatures(this);
    }
    else if (this.value == 'Control') {
        showAdditionalTemperatures(this);
    }
});
$('#configurationForm').submit(function(e){
    e.preventDefault();
});

$(document).ready( function() {
    $('#submitButton').click( function(e) {
        e.preventDefault();

    var object = {};
    // Put form inputs into an array
    var array = $('#configurationForm').children('section');
    array.each(function(){
        if (this.id=='sensors')
        {
            object['sensors']=parseSensors(this);

        }

        else if (this.id == 'system')
        {
            console.log('parsing system');
            object['system'] = $(this).find('select option:selected').val();
        }
        else
        {
            console.log('An error occured during parsing object: '+this);
        }

    });
    object['stereo']={1: $("#mono_stereo-1").prop('checked'),
                      2: $("#mono_stereo-2").prop('checked')};

    $.ajax({
        contentType: 'application/json; charset=UTF-8',
        dataType: 'json',
        data: JSON.stringify(object),
        type: 'POST',
        url: ''
}).done(function(data, textStatus, jqXHR) {
    console.log('Data sent.');
    location.reload()
}).fail(function(jqXHR, textStatus, errorThrown) {
    console.log('There was an error: '+textStatus+'; '+errorThrown);
});

return true;

});
    do_thing=false;
    $.each(config_dict.sensors, fill_sensor_data);
    $.each(config_dict.stereo, set_mono_stereo);
    do_thing=true;
});

function parseSensors(sec){
    var obj = {};
    var sensors = $(sec).find('fieldset');
    sensors.each(function(){
        obj[8-parseInt(this.id.slice(-1))] = parseSensor(this);

    });
    return obj;
}

function parseSensor(sensor){
    var obj={};
    obj['name'] = $(sensor).find('input[id^=name]').val();
    obj['type'] = $(sensor).children('select').children('option:selected').text();

    obj['tset'] = parseFloat($(sensor).find('input[type=number][name^=tSet]').val());
    if ($(sensor).children('input[type=radio][name^=role]:checked').val() == 'Control'){
        obj['thigh'] = parseFloat($(sensor).find('input[type=number][name^=tHigh]').val());
        obj['tlow'] = parseFloat($(sensor).find('input[type=number][name^=tLow]').val());
        obj['role'] = 'Control';
    }
    else
    {
        obj['role'] = 'Observe';
    }
    return obj;
}


function createSensorFieldset(caller){
    var id = parseInt(caller.getAttribute("id").slice(-1));


    if(caller.checked){
        var newChild = createFieldset(id);
        var newChildren = $("#selectedSensors").children();
        newChildren.push(newChild);

        if (do_thing && ((id==5 && $("#mono_stereo-1").prop('checked'))
            || (id==7 && $("#mono_stereo-2").prop('checked'))))
        {
            newChildren.push(createFieldset(id+1));
            $("#port-"+(id+1)).prop("checked",true);

        }

    newChildren.sort(function (a,b){
            return parseInt(a.id.slice(-1))>parseInt(b.id.slice(-1));
        });
    $("#selectedSensors").empty();
    $("#selectedSensors").append(newChildren);

}
else
{
    if ((id==5 && $("#mono_stereo-1").prop('checked'))
        || (id==7 && $("#mono_stereo-2").prop('checked')))
    {
        var obj = document.getElementById(("sensorFieldsetPort"+(id+1)));
        obj.parentNode.removeChild(obj);
        $("#port-"+(id+1)).prop("checked",false);
    }
    var obj = document.getElementById(("sensorFieldsetPort"+id));
    obj.parentNode.removeChild(obj);
}


}


function createFieldset(id){
    var fieldset = $(sensorTemplate.replace(/#ID/g, id))[0];
    // replace ##SensorSelection##
    if (id==0)
    {
        $.each(digital_sensors, function(i, key) {
           $(fieldset).children('[name^=type]')
           .append($('<option>', { key : key })
                   .text(key));
        });
    }
    else
    {
        $.each(analog_sensors, function(i, key) {
           $(fieldset).children('[name^=type]')
           .append($('<option>', { key : key })
                   .text(key));
        });
    }
    return fieldset;

}

function showAdditionalTemperatures(caller){
    var callerId = caller.getAttribute("id");
    var id = callerId.slice(-1);
    var fieldset = $(caller.parentNode);

    fieldset.append($(additionalTempsTemplate.replace(/#ID/g, id)));
}

function removeAdditionalTemperatures(caller){
    var callerId = caller.getAttribute("id");
    var id = callerId.slice(-1);
    var fieldset = caller.parentNode;

    var labelTHigh  = document.getElementById("labelTHigh" +id)
    var sensorTHigh = document.getElementById("tHigh" +id)
    var labelTLow   = document.getElementById("labelTLow" +id)
    var sensorTLow  = document.getElementById("tLow" +id)


    fieldset.removeChild(labelTHigh);
    fieldset.removeChild(sensorTHigh);

    fieldset.removeChild(labelTLow);
    fieldset.removeChild(sensorTLow);
}

function set_digital_sensors(name){
    digital_sensors.push(name);
}
function set_analog_sensors(name){
    analog_sensors.push(name);
}

function set_config_dict(d){
    console.log(d);
    config_dict = JSON.parse(d.replace(/&#39;/g,"\"")
                             .replace(/None/g,"null")
                             .replace(/False/g,'false')
                             .replace(/True/g,'true'));
    console.log(config_dict);
}

function fill_sensor_data(index, value){
    index = 8-index;
    $("#port-"+index).prop('checked',true);
    createSensorFieldset($("#port-"+index)[0]);
    $("#sensorTypeSelection"+index).val(value['type']);
    $("#name"+index).val(value['name']);
    $("#tSet"+index).val(value['tset']);
    if (value['role'] == 'Control'){
        $("#controlRole"+index).prop('checked',true);
        showAdditionalTemperatures($("#controlRole"+index)[0]);
        $("#tLow"+index).val(value['tlow']);
        $("#tHigh"+index).val(value['thigh']);
    }
}

function set_mono_stereo(index, value){
    o1 = $('#mono_stereo-'+index);
    o1.prop('checked',value);
    var id = ('#port-'+(index*2)+4);
    o2 = $(id);
    if (value==true){

        o2.prop('checked',value);
        o2.prop('disabled',true);
    }
    else{
               o2.prop('disabled',false);
    }

}
