import pygal
import numpy as np

from sensors import ResistiveSensor

lines = []
with open('readings', 'r') as f:
    for line in f.readlines():
        t_r, r_r = line.strip().split('; ')
        lines.append((float(t_r), float(r_r)))
t_raw = [x + 273.15 for (x, y) in lines]
r_raw = [y for (x, y) in lines]

t0 = t_raw[::2]
t1 = t_raw[1::2]
r0 = r_raw[::2]
r1 = r_raw[1::2]

newSensor = ResistiveSensor('', 0, [0] * 5)
for (a, b) in zip(t0 + t1, r1 + r0):
    newSensor.calibrationReading(a, b)

newSensor.ridgeRegression(1e-6)
# print(np.linalg.inv(np.matrix(newSensor._phi)) *
print(newSensor.c)
new_values = newSensor._phi * newSensor.c
old_values = np.matrix(t0 + t1)
error = old_values.T - new_values
error = error.T.tolist()[0]

squared_error = [a**2 for a in error]
rmse = np.sqrt(np.mean(squared_error))

print("RMSE: ", rmse)
print(newSensor.to_json())


# plt = pygal.XY(stroke=False)
# plt = pygal.Bar()
# plt.title = 'Temperature Resistance'
# plt.add('A', zip(x0,y0))
# plt.add('B',zip(x1,y1))
# plt.render_to_png('out1.png')
# r_new = range(2000, int(max(y)), 100)
# y_new = [newSensor.resistance2temp(r) for r in r_new]

# plt.render()
# plt = pygal.XY(stroke=False)
# plt.add('A', zip(y0[::2], x1[::2]))
# plt.add('B', zip(r_new, y_new))
# plt.render_to_png('out_switched.png')
