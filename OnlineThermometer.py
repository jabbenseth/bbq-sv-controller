from taskThread import TaskThread
import logging
from tools.timetools import TimeAlarm


import pushbullet as pb

from LocalStorage import DataStorageLocal as DataStorage
from TemperaturePlot import ToyplotPlot

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class OnlineThermometer(TaskThread):

    def __init__(self):

        self.sensors = []
        self.controller = None

        # Timer for alarm
        self.alarms = []

        # output for webinterface

        self.datastore = DataStorage()
        self.plot = ToyplotPlot(self.datastore)
        super(OnlineThermometer, self).__init__()
        self._TaskThread__interval = 5

        self.push_notifications = True
        self.rePushInterval = 60
        self.rePush = -1

    def append_sensors(self, sensors):
        if type(sensors) is list:
            self.sensors += sensors
        else:
            self.sensors += [sensors]

    def set_controller(self, controller):
        self.controller = controller

    def notify(self):
        if not self.push_notifications:
            return
        if self.rePush > 0:
            self.rePush -= 1
            return

        prefix = "bbq-sv-controller: "
        title = "Temperatur Information"
        msg = ""
        for sensor in self.sensors:
            if self.datastore.get_role(sensor.name) == "Observe":
                target = self.datastore.get_most_recent_target(sensor.name)

                try:
                    difference = round(self.datastore.get_most_recent_value(sensor.name) -
                                       target)
                except:
                    logger.info('No target set for %s' % sensor.name)
                    continue
                if difference == 0:
                    msg += "%s hat die eingestellte Temperatur von %d Grad C erreicht.\n" % (
                        sensor.name, target)
                elif abs(difference) <= 2:
                    msg += "%s ist %d Grad C %s der eingestellten Temperatur von %d Grad C\n" % (
                        sensor.name, abs(difference), "ueber" if difference > 0 else "unter", target)

        for alarm in self.alarms:
            if alarm.remaining() < 5 and alarm.remaining() >=0:
                if msg == "":
                    title = "Zeit Information"
                else:
                    title = "Zeit/"+title
                msg += "Verbleibende Zeit: %s" %alarm
        if msg != "":
            self.rePush = int(self.rePushInterval / self._TaskThread__interval)
            note = pb.Note(prefix + title, msg)
            note.push()

    def updateTemperatures(self):
        for s in self.sensors:
            self.datastore.append_datapoint(s.id, s.readTemperature())

    def run(self):
        while 1:

            self.running = True
            if self._TaskThread__finished.isSet():
                self.running = False
                return

            self.updateTemperatures()
            self.notify()

            if self.controller:
                p = self.controller.timestep()
                self.controller.actor.enable()
                self._TaskThread__finished.wait(p * self._TaskThread__interval)
                self.controller.actor.disable()
                self._TaskThread__finished.wait(
                    (1 - p) * self._TaskThread__interval)
            else:
                self._TaskThread__finished.wait(self._TaskThread__interval)
            logger.info('1 iteration run')

    def start(self):
        logger.info('Starting Process')
        if len(self.sensors) is 0:
            raise(ValueError("no Sensor set"))
        else:
            super(OnlineThermometer, self).start()

    def getLastTemperaturesWithFeatures(self):
        retDict = {}

        for sensor in self.datastore.get_keys():
            times, tList = self.datastore.get_values(sensor)
            if len(tList) > 0:
                retDict[sensor] = {
                    'temp': str(tList[-1]),
                    'state': '',
                    'min': str(min(tList)),
                    'max': str(max(tList))}

                target = self.datastore.get_most_recent_target(sensor)

                if target is not None:
                    retDict[sensor]['target'] = str(target)
                else:
                    retDict[sensor]['target'] = None
            else:

                retDict[sensor] = {
                    'temp': '-',
                    'state': '',
                    'min': '-',
                    'max': '-',
                    'target': None
                }

        return retDict
