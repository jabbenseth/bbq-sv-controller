import numpy as np
import logging

import tools.temperaturetools as tt


class Controller(object):

    def __init__(self):
        self.target = None

    def getControl(self, isValue):
        raise NotImplementedError('specify controller type')

    def getNormalizedControl(self, isValue):
        un = self.getControl(isValue)
        n = max(0, np.tanh(un))
        logging.debug("is value: %f, target: %f" % (isValue, self.target))
        logging.debug("Set to %6.4f, control value was %6.2f" % (n, un))
        return n


class pController(Controller):

    def __init__(self, p):
        super(pController, self).__init__()
        self.p = p

    def getControl(self, isValue):
        return self.p * self.getDifference(isValue)

    def getDifference(self, isValue):
        return self.target - isValue


class pdController(pController):

    def __init__(self, p, d):
        super(pdController, self).__init__(p)

        self.d = d

        self.oldValue = None

    def getControl(self, isValue):
        try:
            u = self.d * (isValue - self.oldValue)
        except TypeError:
            u = 0
        u += super(pdController, self).getControl(isValue)
        self.oldValue = isValue
        return u


class pidController(pdController):

    @classmethod
    def fromDict(cls, d):
        return cls(float(d["kp"]), float(d["kd"]), float(d["ki"]))

    def __init__(self, p, d, i):
        super(pidController, self).__init__(p, d)

        self.i = i

        self.integral = 0
        logging.debug(
            'Createt PID controller with pid = %f, %f, %f' % (p, i, d))

    def getControl(self, isValue):
        self.integral += self.getDifference(isValue)

        u = self.i * self.integral
        u += super(pidController, self).getControl(isValue)
        return u


class TemperatureController(object):

    def __init__(self, baseController):
        self.controller = baseController


class TemperatureControllerWithActor(TemperatureController):

    def __init__(self, baseController, datastore, sensor, actor):
        super(TemperatureControllerWithActor, self).__init__(
            baseController)
        self.datastore = datastore
        self.sensor = sensor
        self.actor = actor

    def timestep(self):
        tIs = self.sensor.read_temperature()
        self.controller.target = self.datastore.get_most_recent_target(
            self.sensor.name)
        return self.controller.getNormalizedControl(tIs)


def setup_pController():
    p = 12
    c = pController(p)
    c.target = 100
    return c


def setup_pdController():
    p = 12
    d = 1
    c = pdController(p, d)
    c.target = 100
    return c


def test_pControlDifference():
    c = setup_pController()
    assert (c.getDifference(0) == 100)


def test_pControl():
    c = setup_pController()
    assert (c.getControl(0) == c.p * 100)


def test_pdControlDifference():
    print(test_pdControlDifference)
    c = setup_pdController()
    assert (c.getDifference(0) == 100)


def test_pdControl():
    print(test_pdControl)
    c = setup_pdController()
    assert (c.getControl(0) == c.p * 100)


if __name__ == '__main__':
    test_pControlDifference()
    test_pControl()
    test_pdControlDifference()
    test_pdControl()
