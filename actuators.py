try:
    import RPi.GPIO as GPIO
except RuntimeError:
    from tools.offline import GPIO


def pickActuator(d):
    if d is None:
        return Actuator()
    elif d == 'immersion':
        return RelaisSwitch()
    elif d == 'fan':
        return Actuator()


class Actuator(object):

    def __init__(self, pin=17):
        self.pin = pin
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin, GPIO.OUT)

    def enable(self):
        pass

    def disable(self):
        pass


class RelaisSwitch(Actuator):

    def __init__(self, pin=17):
        super(RelaisSwitch, self).__init__(pin)
        self.disable()

    def enable(self):
        GPIO.output(self.pin, False)

    def disable(self):
        GPIO.output(self.pin, True)
