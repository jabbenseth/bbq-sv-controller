import logging
import requests
import json

from os.path import expanduser

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Note(object):
    pbtype = "note"
    url = 'https://api.pushbullet.com/v2/pushes'
    cred = None
    with open(expanduser('~') + '/' + '.pushbullet', 'r') as f:
        cred = f.read().splitlines()[0]
    if cred is None:
        raise ValueError('No access token supplied')
    headers = {'Content-type': 'application/json'}

    def __init__(self, title, body):
        self.title = title
        self.body = body

    def push(self):
        payload = {'type': self.pbtype, 'title': self.title, 'body': self.body}
        r = requests.post(self.url,
                          auth=(self.cred, ''),
                          headers=self.headers,
                          data=json.dumps(payload))
        logger.info(r)


if __name__ == '__main__':
    n = Note(
        'Hallo Welt', 'ich wurde gerade von einem Python programm gepusht.')
    n.push()
