import time
import pygal

import matplotlib.pyplot as plt
from matplotlib import dates

import toyplot
from toyplot import svg as toysvg
import datetime
import numpy as np
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__file__)


class PygalPlot(object):

    def __init__(self, datastore, refresh=10):
        self.datastore = datastore
        self.last_update = -1
        self.refresh = refresh

    def getPlot(self):
        if (self.last_update + self.refresh) < time.time():
            line_chart = pygal.Line(
                fill=True, style=pygal.style.NeonStyle)
            (timestamps, temperatureList) = self.datastore.get_alldata()
            for sensor in self.datastore.keys:
                index = self.datastore.get_index(sensor)
                temps = [t[index] for t in temperatureList]
                if len(temps) > 0:
                    stepping = int(len(temps) / 50) + 1
                    line_chart.add(
                        sensor, temps[::stepping])

            line_chart.render_to_file('static/images/tempchart.svg')
        self.last_update = time.time()
        return True

    def __call__(self):
        return self.getPlot()


class MatplotlibPlot(object):

    def __init__(self, datastore, refresh=2, show_relative=True):
        self.datastore = datastore
        self.last_update = -1
        self.refresh = refresh
        self.show_relative = show_relative

    def getPlot(self):
        if (self.last_update + self.refresh) < time.time():
            fig = plt.figure()
            ax = fig.add_subplot(111)

            (timestamps, temperatureList) = self.datastore.get_alldata()
            timestamps = np.array(timestamps)
            if self.show_relative:
                timestamps = timestamps - timestamps[0] + (23 * 60 * 60)
            dts = np.array(
                list(map(datetime.datetime.fromtimestamp, timestamps)))

            hfmt = dates.DateFormatter('%H:%M')

            for sensor in self.datastore.keys:
                index = self.datastore.get_index(sensor)
                temps = np.array([t[index] for t in temperatureList])
                mask = np.isfinite(temps)
                if sum(mask) > 1:
                    handle = ax.plot(dts[mask], temps[mask], label=sensor)

            ax.legend(loc=2)
            ax.xaxis.set_major_locator(
                dates.MinuteLocator(range(0, 60, 5)))
            ax.xaxis.set_major_formatter(hfmt)

            ax.set_ylim(bottom=0)

            plt.xticks(rotation=70)

            plt.subplots_adjust(bottom=.1)
            plt.savefig(
                'static/images/tempchart.svg', figsize=(192, 108), dpi=10)

        self.last_update = time.time()
        return True

    def __call__(self):
        return self.getPlot()


class ToyplotPlot(object):

    def __init__(self, datastore, refresh=2, show_relative=True):
        self.datastore = datastore
        self.last_update = -1
        self.refresh = refresh
        self.show_relative = show_relative

    def getPlot(self):
        if (self.last_update + self.refresh) < time.time():
            alldata = self.datastore.get_values()
            canvas = toyplot.Canvas(width=700, height=400, autorender=False)
            axes = canvas.axes()
            legend = {}
            for key, value in sorted(alldata.items()):
                legend[key] = axes.plot(value[0], value[1])

            canvas.legend(legend.items(), corner=('top-left', 100, 100, 20))

            toysvg.render(canvas, fobj="static/images/tempchart.svg")

        self.last_update = time.time()
        return True

    def __call__(self):
        return self.getPlot()


def main():
    from DataStorage import DataStorage
    ds = DataStorage({'name': 'Pulled pork'})
    t = ToyplotPlot(ds)
    t()
if __name__ == '__main__':
    main()
