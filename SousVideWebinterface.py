import logging

from OnlineThermometer import OnlineThermometer

from sensors import pickSensor, MultiTempSensor
from actuators import pickActuator
from controller import TemperatureControllerWithActor, pidController


# from tornado.ioloop import IOLoop
from tornado.web import RequestHandler

import glob
import json

from tools.debugtools import Timer
from tools.timetools import str2min


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class SousVideWebinterface(RequestHandler):

    def initializeParameters(self, path, param):
        paramPaths = glob.glob(path)
        self.database[param] = {}
        self.database['%sName' % param] = []
        for paPa in paramPaths:
            logger.debug('opening: ' + paPa)
            with open(paPa, 'r') as f:
                d = json.load(f)
                self.database[param][d["Name"]] = d
                self.database['%sName' % param].append(d["Name"])

    def initialize(self, database):
        self.database = database

        self.initializeParameters(
            "./config/controller/*.json", 'controlParams')

        self.initializeParameters("./config/food/*.json", 'foodParams')
        self.initializeParameters(
            "./config/sensors/analog/*.json", "analog_sensors")
        self.initializeParameters(
            "./config/sensors/digital/*.json", "digital_sensors")

    def get(self):
        print(self.request)
        if self.database['Thermo'] is not None:
            '''
            Object for controll exists. Display that
            '''
            data = {}
            data['time'] = self.database['Thermo'].alarm.remaining_text()
            with Timer('Features') as t:
                data['Sensors'] = self.database[
                    'Thermo'].getLastTemperaturesWithFeatures()

            data['push_notifications'] = self.database[
                'Thermo'].push_notifications
            if self.database['debug']:
                with Timer("painting") as t:
                    self.database['Thermo'].plot()
            else:
                self.database['Thermo'].plot()
            self.render(
                "templates/HeaterTemplate.html", title="SousVide", data=data)

        else:
            data = {
                "controller_parameters": self.database['controlParamsName'],
                "food": self.database['foodParamsName'],
                "analog_sensors": self.database['analog_sensors'],
                "digital_sensors": self.database['digital_sensors'],

            }

            self.render(
                "templates/SelectionTemplate.html", title="SousVide", data=data)

    def post(self):

        d = json.loads(self.request.body.decode('utf-8'))

        if self.database['Thermo'] is not None:
            self.database['Thermo'].push_notifications = not self.database[
                'Thermo'].push_notifications

        controlSensors = []
        observeSensors = []
        Thermo = OnlineThermometer()
        for name, s in d['sensors'].items():
            print(s)
            sensor = pickSensor(
                s['type'].replace('\n', ''), s['name'], int(name[-1]))
            if s['tset'] is not None:
                Thermo.datastore.set_target(s['name'], s['tset'])
            if s['role'] == 'Control':
                controlSensors.append(sensor)
            elif s['role'] == 'Observe':
                observeSensors.append(sensor)
            Thermo.datastore.set_role(s['name'], s['role'])
            # elif s['task'] == 'food':
            #     foodSensors.append(sensor)
            # elif s['task'] == 'auxiliary':
            #     additionalSensors.append(sensor)

        if len(controlSensors) > 0:
            s = MultiTempSensor(Thermo.datastore, controlSensors)
            Thermo.datastore.set_role(s['name'], 'Control')
            Thermo.datastore.set_target(s['name'], d['t_control'])

            controllerParams = self.database['controlParams'][
                d['system']]
            actuator = pickActuator(controllerParams['actuator'])
            baseController = pidController.fromDict(
                controllerParams)
            controller = TemperatureControllerWithActor(
                baseController, Thermo.datastore, s, actuator)

        else:
            controller = None

        Thermo.append_sensors(observeSensors + controlSensors)
        Thermo.set_controller(controller)

        Thermo.set_alarm(str2min(d['time']))
        logger.info('starting process....')
        Thermo.start()
        self.database["Thermo"] = Thermo
        # self.redirect('/')
        self.write({'success': True})
