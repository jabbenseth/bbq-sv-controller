import logging
from time import sleep
from random import randint
import re
import numpy as np
import json

import sys  # for commandline arguments

import tools.temperaturetools as tt
from mcp3x0x import MCP3208

# logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

MCP = MCP3208(0, 0)


def pickSensor(sensorType, sensorName, port=None):
    log.info('Sensortype: %s' % sensorType)
    if re.match('OneWire', sensorType):
        with open('./config/sensors/digital/%s.json' % sensorType) as f:
            sensorDictionary = json.load(f)
        sensor = DS18B20.fromDict(sensorName, sensorDictionary)

    elif sensorType == 'Test':
        sensor = TestSensor(sensorName)
    else:  # Resistant sensor
        with open('./config/sensors/analog/%s.json' % sensorType) as f:
            sensorDictionary = json.load(f)

        sensor = ResistiveSensor.fromDict(
            sensorName, sensorDictionary, int(port))
    return sensor


class TemperatureSensor(object):

    "Interface for all Temperaturesensors"

    def __init__(self, name='default'):
        if not (isinstance(name, str) or isinstance(name, unicode)) :
            raise TypeError(
                "Sensorname must be of type String. Is: %s, %s" % (type(name), repr(name)))
        self.name = name
        log.info("created sensor '%s'" % name)
        self.temperatures = []

        self._is_control = None
        self.id = self.name

    def temperatureWarning(self):
        return False

    def readTemperatureCelsius(self):
        raise NotImplementedError("readTemperatureCelsius not Implemented")

    def readTemperature(self, update=True):
        return self.readTemperatureCelsius()

    def getState(self):
        return ""


class TestSensor(TemperatureSensor):

    """TestSensor creates a sensor which returns random number between
    0.0 and 300.0"""

    def readTemperatureCelsius(self):
        t = randint(0, 3000) / 10.0
        print ("random temperature %d"%t)
        return t


class DS18B20(TemperatureSensor):

    @classmethod
    def fromDict(cls, name, d):
        log.debug('Create sensor from %s' % str(d))
        return cls(name, id=str(d['id']))

    def __init__(self, name, id='/sys/bus/w1/devices/---/w1_slave'):
        super(DS18B20, self).__init__(name)
        self.id = id
        self.path = '/sys/bus/w1/devices/%s/w1_slave' % self.id
        log.debug('%s: path: %s' % (self.__class__, self.path))

    def readTemperatureCelsius(self):
        value = 999.99
        try:
            with open(self.path, 'r') as f:
                line = f.readline()
                if re.match(r"([0-9a-f]{2} ){9}: crc=[0-9a-f]{2} YES", line):
                    line = f.readline()
                    m = re.match(r"([0-9a-f]{2} ){9}t=([+-]?[0-9]+)", line)
                    if m:
                        value = float(m.group(2)) / 1000.0
                else:
                    log.warn('Sensor reading was not valid')
        except IOError as e:
            log.error('Error reading %s: %s' % (self.path, e))
        return value


class ResistiveSensor(TemperatureSensor):

    @classmethod
    def fromDict(cls, name, d, port=None):
        return cls(name, port, d["c"])

    def __init__(self, name, port, c):
        super(ResistiveSensor, self).__init__(name)
        self.port = port
        self.c = np.matrix(c)
        self.setBaseResistance(port)
        self._y = []
        self._phi = []
        self.id = self.__class__.__name__ + str(self.port)

    def features(self, r):
        return [np.log(r)**i for i in range(self.c.shape[0])]

    def setBaseResistance(self, port):
        with open('config/board/resistors.json', 'r') as f:
            self.base_resistance = json.load(f)[port]
        return
        log.warn(
            'Not yet implemented. tbd: measure resistors,' +
            ' generate config file, read config file')

    def readTemperatureCelsius(self):
        adc = MCP.readadc(self.port)
        return self.adc2temp(adc)

    def adc2Resistance(self, adc):

        # r /(r+base) = adc/4096 -> r = ad * r + ad * base -> r*(1-ad) =
        # ad*base -> r = ad*base/(1-ad)
        ad = adc / ((2**MCP.precision) - 1)
        return ad * self.base_resistance / (1 - ad)

    def resistance2temp(self, r):
        t = (np.matrix(self.features(r)) * self.c) - 273.15
        return int(t[0][0])

    def adc2temp(self, adc):
        return self.resistance2temp(self.adc2Resistance(adc))

    def toDict(self):
        return {'c': self.c.tolist()}

    def readResistance(self):
        return self.adc2Resistance(MCP.readadc(self.port))

    def calibrationReading(self, degC, r=None):
        self._y.append(degC)
        self._phi.append(
            self.features(self.readResistance()if r is None else r))

    def ridgeRegression(self, regularizer=1e-6):
        phi = np.matrix(self._phi)
        log.info("Shape of phi:%d,%d" % phi.shape)
        pInvReg = np.linalg.inv(
            phi.T * phi + (regularizer * np.identity(phi.shape[1]))) * phi.T
        self.c = pInvReg * np.matrix(self._y).T

    def to_json(self):
        d = {'c': self.c.tolist()}
        return json.dumps(d)


class MultiTempSensor(TemperatureSensor):

    def __init__(self, datastore, sensors, weights=None):
        super(MultiTempSensor, self).__init__('artificial_control_sensor')
        self.datastore = datastore
        self.sensors = sensors
        if weights is None:
            self.weights = [1.0 / len(sensors)] * len(sensors)
        elif sum(weights) is not 0:
            assert len(weights) == len(sensors)
            self.weights = map(lambda x: float(x) / sum(weights), weights)
        else:
            assert len(weights) == len(sensors)
            self.weights = weights
        self.id = self.__class__.__name__ + "p" + [s.id for s in self.sensors]

    def read_temperature(self):
        tIs = 0
        for i, s in enumerate(self.sensors):
            temps = self.datastore.get_most_recent_value(s.name)
            tIs += self.weights[i] * temps
        self.datastore.append_datapoint(self.name, tIs)
        return tIs


''' Testing Purpose '''


def main():
    sensor = TestSensor("Test 1")
    print(sensor.readTemperatures())
    s1 = TestSensor('2')
    s2 = MultiTempSensor([sensor, s1])
    for i in range(20):
        s1.readTemperatures()
        sensor.readTemperatures()
        print('s2')
        print(s2.readTemperatures())
    print(sensor.temperatures)


def save_resistance(v1, v2):
    print('recording')
    p = 0
    s1 = ResistiveSensor('S1', p, [0])
    s2 = ResistiveSensor('S2', p + 1, [0])
    s = ''
    for i in range(20):
        s += '%s; %f' % (v1, s1.readResistance())
        s += '\n'
        s += '%s; %f\n' % (v2, s2.readResistance())
        sleep(0.1)
    with open('readings', 'a') as f:
        f.write(s)
    print('done')


if __name__ == '__main__':
    if len(sys.argv) > 1:
        print(str(sys.argv))
        save_resistance(str(sys.argv[1]), str(sys.argv[2]))
    else:
        main()
