import threading


class TaskThread(threading.Thread):

    """."""
    def __init__(self):
        threading.Thread.__init__(self)
        self.__finished = threading.Event()
        self.__interval = 15.0

    def set_interval(self, interval):
        self._interval = interval

    def shutdown(self):
        self.__finished.set()

    def run(self):
        while 1:
            if self.__finished.isSet():
                return
            self.task()

            self.__finished.wait(self.__interval)

    def task(self):
        raise NotImplementedError("Implement task() for it to work")
