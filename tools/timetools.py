from datetime import datetime, timedelta


def str2min(s):
    '''converts string like "hh:mm" in minutes'''
    return sum([int(x) * f for (x, f) in zip(s.split(':'), [60, 1])])


class TimeAlarm(object):

    def __init__(self, name, time):
        now = datetime.now()
        self.alarmtime = now + timedelta(minutes=time)
        self.name = name

    def remaining_minutes(self, now=None):
        if now is None:
            now = datetime.now()
        if (self.alarmtime - now).days < 0:
            return False
        return int((self.alarmtime - now).seconds / 60) % 60

    def remaining_hours(self, now=None):
        if now is None:
            now = datetime.now()
        if (self.alarmtime - now).days < 0:
            return False
        return (int((self.alarmtime - now).seconds / 3600)
                + (self.alarmtime - now).days * 24)

    def remaining_text(self,  now=None):
        return "%02d:%02d" % tuple(self.remaining_time(now))

    def remaining_time(self, now=None):
        if now is None:
            now = datetime.now()
        return [self.remaining_hours(now), self.remaining_minutes(now)]

    def remaining(self, now=None):
        if now is None:
            now = datetime.now()
        if (self.alarmtime - now).days < 0:
            return -1
        return sum([x * f for (x, f) in zip(self.remaining_time(now), [60, 1])])

    def __str__(self):
        return ("Time Alarm with remaining Time (hh:mm): "
                + self.remaining_text())
