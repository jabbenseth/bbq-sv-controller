CELSIUS = 0
FARENHEIT = 1
KELVIN = 2


def getFunctions(unit):
    if unit == CELSIUS:
        return(c2k, k2c)
    elif unit == FARENHEIT:
        return(f2k, k2f)
    else:
        return (lambda x: x, lambda x: x)


def c2f(c):
    """
    Converts a temperature in degree Celsius to degree Farenheit

    Args:
        c (numeric): Temperature in degree Celsius

    """
    return c * 1.8 + 32


def f2c(f):
    """
    Converts a temperature in degree Farenheit to degree Celsius

    Args:
        f (numeric): Temperature in degree Farenheit

    """
    return (f - 32) * 5 / 9


def c2k(c):
    """
    Converts a temperature in degree Celsius to Kelvin

    Args:
        c (numeric): Temperature in degree Celsius

    """
    return c + 273.15


def k2c(k):
    """
    Converts a temperature in Kelvin to degree Celsius

    Args:
        k (numeric): Temperature in Kelvin

    """
    return k - 273.15


def f2k(f):
    """
    Converts a temperature in degree Farenheit to Kelvin

    Args:
        f (numeric): Temperature in degree Farenheit

    """
    return c2k(f2c(f))


def k2f(k):
    """
    Converts a temperature in Kelvin to degree Farenheit

    Args:
        k (numeric): Temperature in Kelvin

    """
    return c2f(k2c(k))
