
import time


class Timer:

    def __init__(self, name):
        self.name = name

    def __enter__(self):
        self.start = time.clock()
        return self

    def __exit__(self, *args):
        self.end = time.clock()
        interval = self.end - self.start
        print('%s took %s seconds' % (self.name, interval))
