import datetime
from pymongo import MongoClient
from numpy import isfinite


CLIENT = MongoClient('pi-openhab', 27017)


def hours_seconds(td):
    return td.days * 24 + (td.seconds // 3600), td.seconds % 3600


def get_last_non_nan(l):
    for x in reversed(l):
        if not isfinite(x):
            return x


class DataStorage(object):

    def __init__(self, query=None):
        self.collection = CLIENT.bbqSvController.logs

        self.values = {}
        self.targets = {}

        self.most_recent_values = {}
        self.most_recent_targets = {}

        self.roles = {}

        if query is None:

            self.start = datetime.datetime.now()
            self.start -= datetime.timedelta(minutes=self.start.minute,
                                             seconds=self.start.second,
                                             microseconds=self.start.microsecond)

            self._id = self.collection.insert_one(
                {"startTime": self.start}).inserted_id

        else:
            selection = self.collection.find(query)
            count = selection.count()
            if count > 1:
                raise KeyError('More than one dataset found')
            elif count == 0:
                raise KeyError('No dataset found')
            self._id = selection[0]['_id']
            self.start = selection[0]['startTime']

            self.update_values_from_database()
            self.update_targets_from_database()
            self.update_roles_from_database()

    def add_tags(self, tags):
        self.collection.update({"_id": self._id},
                               {"$set": tags})

    def append_datapoint(self, key, value):
        hours, seconds = hours_seconds(
            datetime.datetime.now() - self.start)

        newValue = {"values.%s.%d.%d" %
                    (key, hours, seconds): round(value * 1e1)}
        self.collection.update({"_id": self._id},
                               {"$set": newValue})
        if key not in self.values.keys():
            self.values[key] = {}
        self.values[key][(hours * 3600) + seconds] = round(value, 1)
        self.most_recent_values[key] = round(value, 1)

    def update_values_from_database(self):
        data = self.collection.find_one(
            {"_id": self._id}, {"_id": 0, "values": 1})['values']
        for key, values in data.items():
            self.values[key] = {}
            for hour, second_tuple in values.items():
                for second, temp in second_tuple.items():
                    self.values[key][
                        (int(hour) * 3600) + int(second)] = temp / 1e1

    def set_target(self, key, value):
        hours, seconds = hours_seconds(
            datetime.datetime.now() - self.start)
        newValue = {"targets.%s.%d" %
                    (key, (hours * 3600) + seconds): round(value * 1e1)}
        self.collection.update({"_id": self._id},
                               {"$set": newValue})
        if key not in self.targets.keys():
            self.targets[key] = []
        self.targets[key].append(((hours * 3600) + seconds, round(value, 1)))
        self.most_recent_targets[key] = round(value, 1)

    def update_targets_from_database(self):
        data = self.collection.find_one(
            {"_id": self._id}, {"_id": 0, "targets": 1})['targets']
        for key, targets in data.items():
            self.targets[key] = []
            for ts, target in targets.items():
                self.targets[key].append((int(ts), target / 1e1))

    def get_values(self, key=None):
        '''
        returns all readings of sensor specified by key and timestamps
        '''
        if key is None:
            d = {}
            for key in self.values.keys():
                d[key] = self.get_values(key)
            return d
        else:
            time, temp = zip(*sorted(self.values[key].items()))
            return time, temp

    def get_targets(self, key=None):
        '''
        '''
        if key is None:
            d = {}
            for key in self.targets.keys():
                d[key] = self.get_targets(key)
            return d
        else:
            try:
                time, temp = zip(*self.targets[key])
                return time, temp
            except KeyError:
                return None

    def get_keys(self):
        return self.values.keys()

    def get_most_recent_value(self, key):
        try:
            return self.most_recent_values[key]
        except KeyError:
            return None

    def get_most_recent_target(self, key):
        try:
            return self.most_recent_targets[key]
        except KeyError:
            return None

    def set_role(self, key, role):
        newValue = {"roles.%s" %
                    key: role}
        self.collection.update({"_id": self._id},
                               {"$set": newValue})
        self.roles[key] = role
        print("role for %s set to %s" % (key, role))

    def get_role(self, key):
        return self.roles[key]

    def update_roles_from_database(self):
        self.roles = self.collection.find_one(
            {"_id": self._id}, {"_id": 0, "roles": 1})['roles']
