build:
	(cd static/scripts && pub build web)
run:
	python3 main.py

install:
	pip3 install -r requirements.txt
