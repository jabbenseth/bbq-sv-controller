import datetime
import glob

import json


path = "~/DataStorage/bbqSvController/logs"


def hour_seconds(td):
    return td.days * 24 + (td.seconds // 3600), td.seconds % 3600
class DataStorageLocal(object):

    def __init__(self, old_id=None):
        self.json = {'values': {},
                     'targets': {},
                     '_id': None,
                     'tags': {},
                     'start': None,
                     'roles': {}}

        self.most_recent_values = {}
        self.most_recent_targets = {}

        if self.unfinished_job():
            pass

        if old_id is None:
            start = datetime.datetime.now()
            start -= datetime.timedelta(minutes=start.minute,
                                             seconds=start.second,
                                             microseconds=start.microsecond)
            self.json['start'] = start

        else:
            pass

    def append_datapoint(self, key, value):
        if key not in self.json["values"].keys():
            self.json["values"][key]={}
        hours, seconds = hour_seconds(
            datetime.datetime.now() - self.json["start"])

        self.json["values"][key][(hours*3600) + seconds] = round(value *1e1)/10.0

        self.most_recent_values[key] = round(value,1)

    def set_target(self, key, value):
        if key not in self.json["targets"].keys():
            self.json["targets"][key]={}
        hours, seconds = hour_seconds(datetime.datetime.now() - self.json["start"])

        self.json["targets"][key][(hours * 3600) + seconds] = round(value * 1e1)/10.0
        self.most_recent_targets[key] = round(value,1)

    def unfinished_job(self):
        try:
            return glob.glob(path+"/*.running")[0]
        except:
            return None

    def last_id(self):
        with open(glob.glob(path+"/*.bbqlog")[-1], 'r') as f:
            return json.load(f)["_id"]

    def get_values(self, key=None):

        print("Local getValues(%s)"%key)
        print self.json["values"].keys()
        if key is None:
            d = {}
            for key in self.json["values"].keys():
                d[key] = self.get_values(key)
            return d
        else:
            time, temp = zip(*sorted(self.json["values"][key].items()))
            return time,temp
    def get_targets(self, key=None):
        if key is None:
            d={}
            for key in self.json["targets"].keys():
                d[key] = self.get_targets(key)
            return d
        else:
            try:
                time, temp = zip(*self.json["targets"][key])
                return time, temp
            except KeyError:
                return None
    def get_keys(self):
        return self.json["values"].keys()

    def get_most_recent_value(self, key):
       try:
           return self.most_recent_values[key]
       except KeyError:
           return None

    def get_most_recent_target(self, key):
        try:
            return self.most_recent_targets[key]
        except KeyError:
            return None

    def set_role(self, key, role):
        self.json["roles"][key]=role
        print("role for %s set to %s" % (key, role))

    def get_role(self, key):
        return self.json["roles"][key]
