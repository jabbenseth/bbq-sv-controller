[t_raw, r_raw] = textread('readings','%n%n','delimiter','; '); %read file

% Error while collecting data (flipped temperatures)
t=[t_raw(1:2:end);t_raw(2:2:end)]+273.15; % convert °C in Kelvin
r=[r_raw(2:2:end);r_raw(1:2:end)];

% creation of feature vector / Matrix with features of all observed
% resistances

% have a look at https://en.wikipedia.org/wiki/Steinhart-Hart_equation
% I use 5 parameters because of the large region of interest (200K)
features = @(x) [x./x, log(x), log(x).^2, log(x).^3, log(x).^4]; 
Phi = features(r);


r_test = (min(r):100:max(r))';  % for plotting the outcome
%% Fitting Steinhart-Hart
C = Phi\(1./t); % fit with linear regression

t_test = features(r_test)*C;    % compute temperatures for all test resistances
subplot(211)
plot(r./1e3,t, 'rx', r_test./1e3,t_test,'b-');
title('Steinhart-Hart: 1/T = ...')
xlabel('resistance [KOhm]')
ylabel('temperature [K]')

%% Fitting Inverse Steinhart-Hart
subplot(212)
C2 = Phi\t;  % fit with linear regression

t_test2 = features(r_test)*C2;

plot(r./1e3,t, 'rx', r_test./1e3,t_test2,'b-');
title('Inverse Steinhart-Hart: T = ...')
xlabel('resistance [KOhm]')
ylabel('temperature [K]')

