from random import randint
import logging
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


class MCP3X0X(object):

    def __init__(self, bus, device, nPorts, precision):
        self.nPorts = nPorts
        self.precision = precision
        try:
            self.spidev = __import__('spidev')
            self.theRealThing = True
            self.conn = self.spidev.SpiDev(bus, device)
            self.conn.max_speed_hz = 1000000
            log.info('Opened connection via spi:(%d, %d)' %
                     (bus, device))
        except ImportError:
            log.info('Created simulated MCP3X0X')
            self.theRealThing = False
            self.conn = None

    def __del__(self):
        self.close()

    def close(self):
        if self.conn is not None:
            self.conn.close()
            self.conn = None

    def bitstring(self, n):
        s = bin(n)[2:]
        return '0' * (8 - len(s)) + s

    def readadc(self, adc_channel=0):
        # build command
        cmd = 128  # start bit
        cmd += 64  # single end / diff
        cmd += (adc_channel << 3)

        # send & receive data
        reply_bytes = self.conn.xfer2([cmd, 0, 0, 0])

        #
        reply_bitstring = ''.join(self.bitstring(n) for n in reply_bytes)
        # print reply_bitstring

        # see also... http://akizukidenshi.com/download/MCP3204.pdf (page.20)
        reply = reply_bitstring[5:19]
        return int(reply, 2)


class MCP3208(MCP3X0X):

    def __init__(self, bus, device):
        super(MCP3208, self).__init__(bus, device, 8, 12)


class MCP3008(MCP3X0X):

    def __init__(self, bus, device):
        super(MCP3008, self).__init__(bus, device, 8, 10)


class MCP3204(MCP3X0X):

    def __init__(self, bus, device):
        super(MCP3204, self).__init__(bus, device, 4, 12)


class MCP3004(MCP3X0X):

    def __init__(self, bus, device):
        super(MCP3004, self).__init__(bus, device, 4, 10)


if __name__ == '__main__':
    spi = MCP3208(0, 0)

    while True:
        for channel in range(8):
            a = 0
            for i in range(100):
                a += spi.readadc(channel)
            print("ch%d=%04d" % (channel, (a / 100)))
            a = 0
        print('')
